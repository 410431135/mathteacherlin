# 如何用Python傳資料到 Google 試算表

## 前言

因為幫朋友研究Arduino偵測數值實時紀錄到google試算表，於是小研究了一下網路上的教學，這篇最簡單。整理起來提供給需要的人。

若你是用Arduino，請在Python部分那邊跳過，跳到Arduino部分。

註：Arduino部分使用Esp8266。

## Google 試算表部分

首先建立一個空白的Google試算表，到工具->腳本編輯器中。

![截圖 2021-02-28 上午1.20.21](截圖 2021-02-28 上午1.20.21.png)

複製以下程式碼到 .gs的地方

```go
//-----------------------------------------------
//Originally published by Mogsdad@Stackoverflow
//Modified for jarkomdityaz.appspot.com
//-----------------------------------------------
/*

GET request query:
https://script.google.com/macros/s/<gscript id>/exec?tempData=data_here

*/


/* Using spreadsheet API */

function doGet(e) { 
  Logger.log( JSON.stringify(e) );  // view parameters

  var result = 'Ok'; // assume success

  if (e.parameter == undefined) {
    result = 'No Parameters';
  }
  else {
    var id = '<put your Spreadsheet ID here>'; // Spreadsheet ID
    var sheet = SpreadsheetApp.openById(id).getActiveSheet();
    var newRow = sheet.getLastRow() + 1;
    var rowData = [];
    //var waktu = new Date();
    rowData[0] = new Date(); // Timestamp in column A
    for (var param in e.parameter) {
      Logger.log('In for loop, param='+param);
      var value = stripQuotes(e.parameter[param]);
      //Logger.log(param + ':' + e.parameter[param]);
      switch (param) {
        case 'tempData': //Parameter
          rowData[1] = value; //Value in column B
          break;
     // case 'column_C':
       // rowData[2] = value;
       // break;
        default:
          result = "unsupported parameter";
      }
    }
    Logger.log(JSON.stringify(rowData));

    // Write new row below
    var newRange = sheet.getRange(newRow, 1, 1, rowData.length);
    newRange.setValues([rowData]);
  }

  // Return result of operation
  return ContentService.createTextOutput(result);
}

/**
* Remove leading and trailing single or double quotes
*/
function stripQuotes( value ) {
  return value.replace(/^["']|['"]$/g, "");
}
```

注意到第24行，請將你的試算表ID放在這邊。

你的試算表網址中，這一串英文加數字就是試算表的ID

![截圖 2021-02-28 上午1.24.16](截圖 2021-02-28 上午1.24.16.png)

好了之後按部署(Deploy)-> 新建部署![截圖 2021-02-28 上午1.26.43](截圖 2021-02-28 上午1.26.43.png)

選擇「Web應用」填寫描述（隨你寫），以及權限，請選擇「任何人」。

![截圖 2021-02-28 上午1.28.56](截圖 2021-02-28 上午1.28.56.png)

![截圖 2021-02-28 上午1.29.16](截圖 2021-02-28 上午1.29.16.png)

好了之後按「部署」。

進到此畫面，請複製Ｗeb應用的網址。

![截圖 2021-02-28 上午1.31.14](截圖 2021-02-28 上午1.31.14.png)

到這步，已經完成了Google端的設置了！

來測試一下吧！

將剛剛複製的網址後面加上?tempData=123

例如：

https://script.google.com/macros/s/AKfycbyi_9bQlWQNFYY184Ii2Rgq1ATQYG8Zvhyf9OcPZ47OwI5TN4JrB7mt/exec?tempData=123

然後在瀏覽器上面網址列送出這串網址，你會在你的google表單上看到一筆新的資料123以及對應的時間！![截圖 2021-02-28 上午1.57.33](截圖 2021-02-28 上午1.57.33.png)

接著，是Python的部分（其他程式也可以依樣畫葫蘆）

但這之前要先設定一個東西，晚點再解釋原因。

## PushingBox部分

最後再解釋理由，這邊我們先實作。

請到 https://www.pushingbox.com 這個網站進行設置。為什麼要透過這個最後說明。

進入之後可以用Google登入，然後點擊「My Services」->點擊「Add a service」。

![截圖 2021-02-28 上午2.00.10](截圖 2021-02-28 上午2.00.10.png)



選擇最下方的「CustomURL」，點擊「Select this service」。![截圖 2021-02-28 上午2.02.28](截圖 2021-02-28 上午2.02.28.png)

名字隨你填，Root URL就是剛剛我們部署後複製的那個，Method選擇GET。![截圖 2021-02-28 上午2.03.39](截圖 2021-02-28 上午2.03.39.png)

接著點上方「[My Scenarios](https://www.pushingbox.com/scenarios.php)」找到你剛剛新增的東東，進去後點擊「Add an action」。裡面請填

```html
?tempData=$tempData$
```

![截圖 2021-02-28 上午2.06.23](截圖 2021-02-28 上午2.06.23.png)

好了之後按Submit。

接著回到剛剛那邊，我們要複製DeviceID。

![截圖 2021-02-28 上午2.07.55](截圖 2021-02-28 上午2.07.55.png)

接著，我們來做一個測試，

在網址列輸入這個網址，並且把DeviceID改成你自己的：

`http://api.pushingbox.com/pushingbox?devid=vE09CF66B53BF395&tempData=123`

送出之後，你應該會在你的google表單看到新的一筆資料123以及時間。

到這邊，pushing box 的部分就完成。

我們進入Python部分。

## Python部分

```python
import requests

r = requests.get('http://api.pushingbox.com/pushingbox?devid=vE09CF66B53BF395&tempData=123')
```

結束，希望不會太快。

至於怎麼修改資料，就不是本篇的重點，有多種方法，根據你的需求進行修改即可，這邊就介紹最基本可以將訊息傳出。

## Arduino部分

```C
#include <ESP8266WiFi.h>
#include <WiFiClientSecureAxTLS.h>

#define WIFI_SSID "你的wifi名稱"
#define WIFI_PASSWORD "你的wifi密碼"


String msg = "test";

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("連線中");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.print("\n 分配到 IP: ");
  Serial.println(WiFi.localIP());
}

void loop() {

  WiFiClientSecure client;
  client.setInsecure();
  if (!client.connect("api.pushingbox.com", 443)) {
    Serial.println("連線失敗!!!");
    Serial.println(client.status());
    delay(5000);
    return;
  }

  msg = "test";
  String  req = "GET /pushingbox?devid=你自己pushingbox的&tempData=";
  req += msg;
  req += " HTTP/1.1";
  client.println(req);
  client.println("Host: api.pushingbox.com");
  client.println("Connection: close");
  client.println();


  while (client.connected()) {
    String line = client.readStringUntil('\n');
    if (line == "\r")
      break;
  }
}
```

這邊不再贅述，需要改的有最上方wifi名稱、密碼；

以及36行要改成自己的DeviceID。

另外，28行可以把443改成80。

註：443代表https，80代表http。

## 小小解釋

為什麼谷歌那邊設定完了。明明我們可以直接用，卻還要透過PushingBox呢？原因在於谷歌那邊需要用「https」，

我們知道有分http跟https，簡單來說就是一個沒有加密（http），一個有加密（https）。

那麼谷歌這邊需要用有加密的https作為請求才可以，如果我們程式直接用http應該會出錯。

那程式要送出https不是不行，但是既然要「加密」，我們就會有一個「驗證」的步驟。這邊就比較麻煩（但是是可以做到的，有興趣的捧油可以搜尋python https request之類的關鍵字。）

而如果我們只是要送出http就很簡單。如同上面，兩行程式碼搞定。

那pushingbox的目的就是，我們先用http傳資料給pushingbox，再請他幫我們傳一個https給google，搞定。

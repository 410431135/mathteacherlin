# First Course

為讓大部分的人可以快速上手，並且沒有換裝置的問題，因此範例多使用google 的 [Colab](https://colab.research.google.com)。

對於想進一步的人可以在自己的電腦下載[Python3](https://www.python.org)或是直接下載[Anaconda](https://anaconda.org)（推薦）。

若英文還可以的人，建議可以到[SoloLearn](https://www.sololearn.com)加深加廣自學。

另外，想做題目練習的請到[Zero Judge](https://zerojudge.tw)練功。



第一堂課首先介紹Python基本用法，包含**運算子**、**print()函數**。

##Print()

```python
print("Hello World!")
```

{% klipse "eval-python" %}
print("Hello World!")
{% endklipse %}

`"Hello World"`是學程式最基本的東西。`print`代表印出，而在哪印呢？以Colab為例，就是印在下方顯示出來。

`print`這個函數，會印出括號裡面的東西，而`"Hello World!"`就是我們要印的字串，可以改成`"Hello!"`那就會印出`"Hello!"`，

這邊要注意的是，我們要印一個「字串」，例如字母、單字的時候，要用引號包裝起來，如此才能告訴Python 這是一個字串。

## Operators

運算符號在演算法設計上很常使用到，也就是小學學的「加減乘除」、括號、次方。還有兩個是取商數跟取餘數。

```python
# 加法
print(1+1)
# output: 2

# 乘法
print(2*3) 
# output: 6

# 除法
print(3/2)
# output: 1.5

# 減法
print(3-2)
# output: 1
    
# 次方
print(3**2)
# output: 9

# 取商數
print(31//5)
# output: 6

# 取餘數
print(31%5)
# output: 1
```

### Give it a try

{% klipse "eval-python" %}
print(31%5)
{% endklipse %}

「次方」的符號是兩次乘法符號，`3**2`代表3的2次方；

「取商數」意思是整除幾次的意思，例如 `31//5` 代表，31除以5的商數；

「取餘數」就是除完剩下的餘數，例如`31%5`代表31除以5的餘數（＝1）。

至於四則運算的規則跟一般都是一樣的，即「先乘除後加減」、「有括號先算」、「次方先算」，

若是不太確定運算順序，其實也可以直接將想先計算的部分括號起來，那麼必定會先算！



這裡要注意的是，跟剛剛印出Hello World不同，這次我沒有加上雙引號。

原因是因為，加入雙引號代表「字串」，而我們剛剛輸入的是數字運算，因此不用加引號。

但加引號並不會出錯！請**試試看**，若加入引號，輸出結果會有什麼不同？



## 註解

上方範例可以注意到我加了井字號 `#`，這是python裡面「註解」的方式，

註解的意思是，這行字與程式的執行無關，可能是用來對程式進行說明，或是把不需要的功能透過註解來消除。



## 型別(type)、命名(namespace)、宣告(declear)與賦值(assign)

程式語言中，每個東西都有它的「型別」，例如"Hello World"是「字串」、9453是「整數」、`print()`是「函數」等等。

字串的英文是`String`、整數是`int`、浮點數（先理解為小數）是`float`。

為了實際運用的需要，我們會用很多的「變數」，這些變數可以是任何的型別，而要區別這些不同變數，我們則需要去「命名」它。

例如，「令 a = 3」這件事情，我們可以理解為，新增一個變數，名稱是 a，而他的「值」是3。

在程式裡面我們會這樣做：

```python
a = 3
print(a)
# output: 3
print(type(a))
# output: <class 'int'>
print("a")
# output: a
```

### Try it yourself

{% klipse "eval-python" %}
a = 3
print(type(a))
{% endklipse %}

宣告 `a` 是`3`。而我們利用`tpye()`來觀察它的型別，會顯示他是一個整數。

這裡要注意，當我們單純講`a`的時候，它變成3了。但如果我們用引號把它包起來，那它就是一個單純的字母`a`。

*註：與一些程式語言不同，python不用寫 int a = 3，這樣反而會出錯。*



## input()

當我們需要跟程式互動的時候，會想要讓使用者輸入東西，然後程式再根據使用者的輸入，做出相對應的輸出。這時候我們可以使用`input()`這個函數。

```python
a = input("please input an integer: ")
```

如此，當程式執行到這邊的時候，就會停下來，並且出現「`please input an integer: `」然後等待使用者輸入。

```python
a = input("please input an integer: ")
# 輸入8989339
print(a)
# output: '8989339'

print(a-3)
# Traceback (most recent call last):
#  File "<stdin>", line 1, in <module>
# TypeError: unsupported operand type(s) for -: 'str' and 'int'
```

這個地方會注意到一件事，我們明明輸入的是「整數」但輸出卻出現了引號，引號的出現代表它是一個「字串」。

因此當我們做數字的運算的時候，會出現錯誤！讓我們讀一下錯誤訊息：

`TypeError: unsupported operand type(s) for -: 'str' and 'int'`

代表它是一個型別錯誤（TypeError），他說`'str'`（字串）跟`'int'`（整數）不能做`-`減法的運算！

為什麼會這樣呢？Python將`input()`接收到的任何東西都當作字串，然後再讓程式設計者自行去決定怎麼處理它。

因此我們若要把它當作數字來處理，就要先進行型別的轉換！

這裡介紹兩個方法：

1. 轉型別

	```python
	a = '3'
	print(type(a))
	# output: <class 'str'>
	int(a)
	print(type(a))
	# output: <class 'int'>
	```

	這邊我們做的事情就是利用`int()`這個函數，強制將括號內的東西變成整數。

	但要注意的是，若變數a本身內容不是整數，那麼可能就會出錯！試試看令ａ是一個字串，然後轉型別看看吧！

2. `eval()`

	```python
	a = '3'
	eval(a)
	print(type(a))
	# output: <class 'int'>
	```

	`eval()`是一個好用的函數，可以直接理解成「將引號脫掉」。因此這邊若將第一行的a的引號脫掉，自然剩下3，是個整數。

	這裡比轉型別好用在哪呢？若是原本輸入是一個浮點數（小數），而妳強制更改型別成整數的話，那麼小數部分就會被捨去，而身為程式設計者的你可能渾然不知。但若用`eval()`，python可以自然而然幫你判斷成浮點數。



## Format output

這裡我們把剛剛學到的再多加一些，讓你的程式可以做更多事情。我們稍微介紹**格式化輸出**。

我們直接來看一個例子：

```python
age = 16
msg = "Hi, I'm %d years old!" % (age)
print(msg)
# output: "Hi, I'm 16 years old!"
```

我們看一下第二行，`%d`代表的是「這個地方要替換成整數」的意思，而後面 `% (age)`則是要替換的東西。

繼續延伸這個例子：

```python
age = 16
name = 'Kyle'
msg = "Hi, my name is %s. I'm %d years old!" % (name, age)
print(msg)
# output: "Hi, my name is Kyle. I'm 16 years old!"
```

{% klipse "eval-python" %}
age = 0
name = 'some_name'
print("Hi, my name is %s. I'm %d years old!" % (name, age))
{% endklipse %}

剛剛說`%d`代表整數，而現在多了一個`%s`代表字串。

後面的`% (name, age)`代表依照順序替換成變數 name 、age。

## Exercise

試著綜合上述所學，建立一個程式完成以下情境：

讓使用者輸入名字、出生的西元年，然後印出一段包含名字跟「年紀」的簡短自我介紹。程式可視情況增加功能。

(請至少用到，。：、！？等標點符號。)

## Advance Exercise

嘗試做一個「自我介紹生成器」吧！

(請使用到鍵盤上所有的按鍵及符號。)

## Advance Exercise - 2

[a006：一元二次方程式](https://zerojudge.tw/ShowProblem?problemid=a006)

## Summary

來總結一下這堂課學到的東西：

你現在可以讓使用者輸入東西、或是自己宣告變數並且賦值，可以做基本的運算，可以印出東西。

## Try it !

[a001](https://zerojudge.tw/ShowProblem?problemid=a001)

## Reference

[Format output](https://docs.python.org/3/tutorial/inputoutput.html)










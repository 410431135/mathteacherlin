# Second Course

今天要延伸`print()`函數、字串，並教判斷式`if`、 以及`while`迴圈。

重要：可以隨時使用`help()`來研究某個東西怎麼使用！

## print()

第一堂課我們一開始教了`print()`函數，但只有教非常簡單的部分。讓我們看看還有什麼功能！

```python
help(print)
```

![截圖 2020-12-31 上午10.02.15](second-1.png)

讓我們看一下裡面有什麼東西可以用，

上次我們只有輸入`value`，也就是要印出的值。而該函數還有`end`可以使用，意思是，結尾要什麼。

他的預設是`end='\n'`，`'\n'`的意思在程式語言中代表「換行」。因此`end='\n'`就是，結尾自動換行的意思。

有時候我們不希望他換行，可以改成`end=''`，也就是一個空的字串，來取代換行。

[Read More](https://www.runoob.com/python/python-strings.html)

```python
print('Hi')
print('I am Kyle')
# output: 
    Hi
    I am Kyle
print('Hi', end='')
print('I am Kyle', end='')
# output:
    Hi I am Kyle
```

## if

```python
if (condtions):
	do_something
elif (other condtions):
	do_something
else:
	do_something_if_all_above_not_hold
```

`if`判斷條件，若條件成立，就會做裡面的事情，若不符合，則看下一個`elif`代表`else if`。若都不符合，則跳到`else`中。

這邊`if`可以單獨使用，而不搭配`elif`或`else`但要用`elif`或`else`一定要先`if`。

舉例，我們讓使用者輸入一個數字，若數字大於60，就印出「及格！」

```python
score = input("Please input a score: ")
score = eval(score) # 將score 從「字串」轉為「數字」
if (score > 60):
	print("及格！")
```

### Quick exercise

試著將上面這段程式擴充，使得程式有以下功能：

讓使用者輸入一個數字，若數字大於60，就印出「及格！」若小於60但大於40，就印出「不及格，但可以補考」。若低於40，則印出「無法補考了Ｑ」。

### Advance

再延伸，若大於60分時，若大於80分，則印出「超優秀欸！」。若使用者不小心輸入了無法判斷的東西，例如，非輸字；例如，大於一百或小於0。也給使用者提示。

## while loop

```python
while (conditions):
	do_something
```

`while`就是當 ... 成立時，要做什麼事情。做完一次之後，就會再檢查一次條件，若成立就繼續做！

舉例，我們想像某個會場進場時要有人數統計，每當一個人進來後，計數器會+1，當計數器達到20人的時候，就停止進場。

```python
counter = 0 # 令一個新的變數，叫做counter 其值為0
while (counter <= 20):
    counter += 1
    print("Hi! guest number %d" % (counter))
```


### Quick exercise

試著結合`while`迴圈、`if`判斷式，製作一個程式：

使用者先輸入人數。之後每個人要輸入他的成績，若大於等於60，則印出「及格！」；小於60但大於等於40就印出「可補考！」；小於40則印出「要重修了！」

## Print Stars

python 在字串的處裡上，有個特殊的地方，若我們將某個字串乘以正整數n，那麼這個字串會重複n次。

```python
print("*"*3) # output: ***
```

{% klipse "eval-python" %}
print("*"*3)
{% endklipse %}

我們配合while loop ，使用計數器的概念，來印看看高度為5的三角形

```python
n = 1
while (n <= 5):
    print("*"*n)
    n += 1
//output
*
**
***
****
*****
```

### Quick Exercise

練習印出以下各種形狀吧！

```python
shape 1
*
**
***
****

shape 2
****
***
**
*

shape 3
   *
  **
 ***
****

shape 4
****
 ***
  **
   *

shape 5
  *
 ***
*****

shape 6
*****
 ***
  *

shape 7
*
**
***
****
***
**
*

shape 8
   *
  **
 ***
****
 ***
  **
   *
```

### Advance

1. 試試看還有哪些形狀
2. 試試看將形狀進行組合
3. 讓使用者輸入高度n，來變成任意高度的三角形

##random()

這裡介紹一個隨機函數，`random()`

```python
import random
print(random.randint(2,10))
# output: 2~10中某一個整數
print(random.random())
# 0~1之間的小數
print(random.choice(['飯','麵','水餃']))
# 飯、麵、水餃其中一個
```

{% klipse "eval-python" %}
import random
print(random.randint(2,10))
print(random.random())
print(random.choice(['rice','noodle','dumpling']))
{% endklipse %}

`import `就是要導入某個工具箱（函數庫），就像C語言的 #include 一樣。有些我們平常很常用到的工具，不需要`import`，我們稱這些工具（函數）叫做built-in function。而有些不一定會用到的工具，就在使用到的時候才導入，這時候就要`import`。這次我們試著導入「隨機」這個工具箱。`import random`。如此，就可以使用`random`裡面的工具（方法、函數）了！

這邊第三個例子`random.choice(['飯','麵','水餃'])`我們用了list，這個還沒教到，但我們可以簡單觀察，我們在中括號裡面放了三種字串。下一堂課，我們會教`list`這個重要的容器。

## Exercise

試著做一個猜數字遊戲：

先指定數字範圍，例如0~100，

電腦預先選定一個數字，例如80，

使用者開始猜，根據使用者猜的數字縮小範圍，例如使用者猜40，則電腦回傳40~100之間。

若使用者繼續猜95，則電腦回傳40~95之間。直到猜到為止。


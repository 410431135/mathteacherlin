# 匿名傳Line

## 展示影片

[遠距上課_利用Python+Django+Line Notify 做出匿名傳送Line訊息](https://youtu.be/Ts97ORDB58A)

## 開發環境/工具

Python 3.6.3 + Django 2.2.6 + Line Notify + Ngrok

## 緣起

由於班上同學在遠距上課開始後，我問「有沒有問題呀？」、「這邊聽得懂嗎？」非常少人回答。而是後，跟我比較熟的同學會私下跟我說其實當下是不懂的，但都沒有人講，不好意思問。

於是我就想，跟我比較熟的同學都不好意思問了，可能有潛在更多人有這個問題。雖然不好意思問問題這個現象是需要解決的，但是否可以有個方法先「治標不治本」一下呢？

於是就想到我之前接觸的Line Notify功能。只需要申請權杖之後，就可以傳訊息給指定的群組，設定起來很方便。

一開始原本是用Colab，但介面比較不是使用者面向，於是就想到可以用Django來包裝成網站，再用Ngrok來作為臨時的部署（畢竟只需要有課開就好，而且每次使用人數不超過４０ 人。）

## 原始碼分享

[我的Gitlab](https://gitlab.com/410431135/teacherwait)

下載後，更改```Wait```此資料夾裡面的權杖，就可以直接使用了。目前這個版本我還有加上簽到功能，但僅限於我的班級使用。若要彈性調整的話，要再花點時間開發。

## 開發筆記

### Line Notify

首先，請先參考我的[手把手教你Line notify](https://410431135.gitlab.io/mathteacherlin/Python/LineNotify.html)來申請權杖。裡面的程式碼，也會用到。

### Django

這邊就不從安裝開始教，先假設你會Django一些基礎。

新增一個專案，名為teacherwait

> ```django-admin createproject teacherwait``` 

我們新增一個App，名為Wait

> ```python manage.py startapp Wait```

接著我們先來設定路徑url：

到```teacherwait/urls.py```中

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('Wait/', include('Wait.urls')),
]

```

再到```teacher/Wait```中新增一個檔案 ```urls.py```

```python
# urls.py
from django.urls import path
from . import views
 
urlpatterns = [
    path('', views.wait),
]
```

然後到```view.py```中

```python
from django.shortcuts import render
from django.http import HttpResponse
import requests

def LineMessage(token, msg):
    headers = {
        "Authorization": "Bearer "+token,
        "Content-Type":"application/x-www-form-urlencoded"
    }
    payload = {
      'message' : msg,
                    }

    r = requests.post('https://notify-api.line.me/api/notify', headers = headers, params = payload)
    return r.status_code

def wait(request):
	token = '將此處改成您申請的權杖'
	message = '測試'
	status = "未傳送"
	if request.POST != {}:
		message = request.POST['some_words']
		if LineMessage(token, message) == 200:
			status = '你的訊息"' + message + '"傳送成功！'
		else:
			status = "奇怪的原因失敗了QQ"
		return render(request, 'Wait/wait.html', locals())
	return render(request, 'Wait/wait.html', locals())

```

最後新增我們的網頁！

在```teacherwiat/Wait```新增```templates/Wait/wait.html```

```html
<h2>上課有問題馬上跟冠曄說</h2>
<p>輸入你想講的話：</p>
<form  action="" method="POST">{% csrf_token %}
	<input type="text" placeholder="點這邊輸入" name="some_words">
    <button type="submit">送出</button>
</form>
<strong>傳送狀態：</strong> {{status}}

```

這樣子一個超級簡易的匿名傳Line就完成了！

測試一下吧！

```python manage.py runserver```

到瀏覽器輸入```127.0.0.1:8000/Wait```

## ngrok部署

請搜尋[ngrok](https://ngrok.com)看一下如何使用。

指令很簡單，先開啟django然後再開ngork 就可以！

```python manage.py runserver```

```./ngrok http 8000```

然後他會出現一串網址，將那串網址貼給學生就可以囉！

註：預設django 會開在8000，您也可以設定其他埠。
# Forth Course

`for-loop, replace(), split(), sys.stdin()`

##sys.stdin()、replace()、split()

在使用zero judge 或其他線上解題系統時，會遇到幾個問題，包含輸入／輸出、ＥＯＦ（end of file）等等。

以下面這個例子說明：

[a001](https://zerojudge.tw/ShowProblem?problemid=a001)

```python
# a001.py
from sys import stdin
for line in stdin:
	print("hello,", line)
```

`sys`是`system`的意思，而`stdin()`，則是代表`standar input`之意。可以用來取代 `input()`。

此處好處是，當輸入結束時，使用`stdin`會自動結束。但是若用`input()`，系統會一直等待，最終造成回應時間過長。

此外，在輸入時，根據不同系統的電腦，會自動加入一些符號。來當作「換行」、「回車」。

「換行」應該已經不陌生，就是`\n`。

「回車」是`\r`。他的概念是早期打字機，打完一行時，除了要「換到下一行」，還要將機器「拉回」到文件最左方，這個動作便是「回車」。

因此，我們平常在電腦看到的字，可能就包含了這兩個東西。舉例而言：

```bash
Hi! My name is Kyle.
I teach both computer science and mathematics.
# 實際上的樣子是
Hi! My name is Kyle.\r\n
I teach both computer science and mathematics.\r\n
```

當我們讓使用者輸入時，很可能就連帶著一起輸入這兩個東西。我們在做相關的演算時，要先將其排除。

這時候就會用到`replace()`

```python
"some string".replace('\r', '').replace('\n', '')
```

`replace()`是字串的內建方法，功用是將字串內的特定字替換掉。本例中，搜尋`\r`，並將其換成空字串`''`，搜尋`\n`，並替換成空字串`''`。


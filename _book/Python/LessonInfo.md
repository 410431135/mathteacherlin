# 110學年度第二學期--Python微課程--第一期課程資訊

## Leaderboard

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vT6G4fJKgp3Tk_ZjfQHDDXBK_0uiy2vwbXvud2Qxa8EWS1Xe_P2v56F40-z587HLPH1u9mp00uRxhIw/pubhtml?gid=987367271&amp;single=true&amp;widget=true&amp;headers=false" width="480" height="480"></iframe>

## Line 群組 QR Code

![IMG_2266](IMG_2266.JPG)

## 上課使用的資源

1. [Google Colab](https://colab.research.google.com)
2. [交作業](https://docs.google.com/spreadsheets/d/1ipaXabXFpsXyCb0WilcCiMSPAGStBwdg1p5m-DT_Y4w/edit?usp=sharing)
3. [Zero Judge](https://colab.research.google.com)
4. [Solo Learn](https://www.sololearn.com)
5. [W3 School](http://w3schools.com)
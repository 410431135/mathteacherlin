# 108國中教育會考數學科

> Author : 林冠曄
>
> First edition : 2019/11/27
>
> Last update : 2019/11/27
>
> [原始題目pdf](https://gitlab.com/410431135/mathteacherlin/raw/master/數學大考題/國中數學/108教育會考數學科/108P_Math150DPI.pdf?inline=false)

## 第一部分：選擇題（1~26題）

| 1. 算式 $$-\frac{5}{3} - (- \frac{1}{6})$$ 之值為何？        |
| ------------------------------------------------------------ |
| (A) $$-\frac32$$  (B) $$-\frac43$$  (C) $$-\frac{11}6$$  (D) $$-\frac49$$ |

**答**：(A)

**解**：原式$$= \frac16(-10+1)=-\frac32$$

> ~~打一題之後看到圖表開始懶得打字~~

> **[info] Tips**
>
> 將分母提出，分子部分通分後就相當於做「整數」的計算！

![第2題](https://imgur.com/xKx8tDj.png)

**答**：(A)

**解**：

105年總數量：$$1250+1000=2250$$

106年總數量：$$2400+2000=4400$$

107年總數量：$$2000+3200=5200$$

$$\because ~ 2250<4400<5200, ~\therefore$$ 逐年增加

> **[info] Tips**
>
> 雖然這題圖形明顯，但建議還是實際計算出來比較保險哦！

![第3題](https://imgur.com/WKOz05I.png)

**答**：(D)

**解**：根據分配律，原式$$= (2\cdot 3 )x^2 + (2\cdot 4- 3\cdot 3)x +(-3\cdot4) = 6x^2-x-12$$

> **[info] Tips**
>
> 把握「分配律」的原則，每一項都要乘到，而後「同類項合併」，注意「正負號」。

![第4題](https://imgur.com/YpYMITb.png)

**答**：(C)

**解**：
矩形：左2+右2+下2 $$=6 \cdot b = 6b$$

正三角形：$$a \times 4 \times \text{(前+後)}=8a$$

$$\therefore~8a+6b $$

![第5題](https://imgur.com/En8fjNh.png)

**答**：(B)

**解**：

等號兩邊同時平方，得

1. $$(\sqrt{44})^2 = (2\sqrt{a})^2 \quad \Rightarrow ~44=4a, ~\therefore ~a=11$$
2. $$(\sqrt{54})^2 = (3\sqrt{b})^2 \quad ~ \Rightarrow 54 = 9b,~\therefore b=6$$

$$\therefore ~ a+b= 11+6 =17$$

> **[info] Tips**
>
> 「根號」不容易計算，所以我們利用「同時平方」化成「整數」

![第6題](https://imgur.com/8BaZBIT.png)

**答**：(C)

**解**：

一萬 $$= 10000 = 10^4$$, 千瓦 $$= 1000 = 10^3$$ (瓦)

$$\therefore ~430 \text{萬千瓦} = 430\times 10^4 \times 10^3 = 4.3\times 10^{2+4+3}=4.3 \times 10^9$$

> **[info] Tips**
>
> 將文字轉換為數字

![第7題](https://imgur.com/4UQNFxx.png)

**答**：(D)

**解**：$$L$$ 為 $$y=4$$，通過$$D (4,4)$$

![第8題](https://imgur.com/GS7rmuS.png)

**答**：(A)

**解**：根據提示，因式分解為 $$(x+a)(5x+c ) = (x+4)(5x-3)$$

$$\therefore ~ a=4,~b=5,~c=-3\quad \therefore a+c=4-3=1$$

![第9題](https://imgur.com/b9P4sqR.png)

**答**：(A)

**解**：

1. 觀察當正方形使用 1 個的時候：
	1. 正方形有 1 個
	2. 三角形有 $$1 + 2\times 2 +1 =6$$ 個（由左而右計算，頭 + (上下)$$\times 2$$ 組 + 尾）
2. 觀察當正方形使用 2 個的時候：
	1. 正方形有 2 個
	2. 三角形有 $$1 + 2 \times 3 +1=8$$ 個（由左而右計算，頭 + (上下)$$\times 3$$ 組 + 尾）
3. 推測當正方形使用 n 個的時候：
	1. 正方形有 n 個
	2. 三角形有 $$1+2\times (n+1) + 1 = 2n+4$$ 個（由左而右計算，頭 + (上下)$$\times (n+1)$$ 組 + 尾）
4. 則當$$n=40$$，$$2n+4=2\times 40+4=84$$

> **[info] Tips**
>
> 大的數字不容易觀察；透過小數字先觀察，並找規律！

![第10題](https://imgur.com/fdoXkh6.png)

**答**：(D)

**解**：$$|d-5| = |d-c|$$ 表示：$$d \text{ 到 }5 \text{ 的距離 }= d \text{ 到 }c\text{ 的距離}$$

$$\therefore$$ 在$$C, ~B$$ 取中點，得 $$D$$ 點介於 $$O, ~B$$ 之間

> **[info] Tips**
>
> 「絕對值」的幾何意義為 兩點間的距離，例如：
>
> $$|a-3|$$ 表示，$$a$$ 跟 $$3$$ 的距離，與 $$|3-a|$$ 相等
>
> $$|a+3|$$ 意思是 $$|a-(-3)|$$ 表示，$$a$$ 跟 $$-3$$ 的距離，與 $$|-3-a|$$ 相等

![第11題](https://imgur.com/jhA12cG.png)

**答**：(C)

**解**：~~此題要補圖~~



![第12題](https://imgur.com/aZCmrhb.png)

**答**：(D)

**解**：設桂圓蛋糕 $$x$$ 個，金棗蛋糕 $$y$$ 個，則總價為 $$350x+200y$$

1. 購買 $$10$$ 盒 $$\Rightarrow ~x+y=10$$
2. 總價不超過$$2500 \Rightarrow~ 350x+200y < 2500 \Rightarrow~7x+4y<50$$
3. 分給 $$75$$ 人，每個人至少一個 $$12x+6y \geq 75 \Rightarrow~4x+2y \geq 25$$

|   $$x$$   |       $$10$$       |        $$9$$         |        $$8$$         |       $$7$$        |        $$6$$        |       $$5$$        |       $$4$$        |         $$3$$          |       $$2$$       |       $$1$$       |       $$0$$       |
| :-------: | :----------------: | :------------------: | :------------------: | :----------------: | :-----------------: | :----------------: | :----------------: | :--------------------: | :---------------: | :---------------: | :---------------: |
|   $$y$$   |       $$0$$        |        $$1$$         |        $$2$$         |       $$3$$        |        $$4$$        |       $$5$$        |       $$6$$        |       **$$7$$**        |       $$8$$       |       $$9$$       |      $$10$$       |
| $$7x+4y$$ | $$70>50$$, 不符合  |  $$67>50$$, 不符合   |  $$64>50$$, 不符合   | $$61>50$$, 不符合  | $$58> 50$$, 不符合  | $$55>50$$, 不符合  | $$52>50$$, 不符合  |  **$$49<50$$, 符合**   |  $$46<50$$, 符合  |  $$43<50$$, 符合  |  $$40<50$$, 符合  |
| $$4x+2y$$ | $$40\geq25$$, 符合 | $$38 \geq 25$$, 符合 | $$36 \geq 25$$, 符合 | $$34\geq25$$, 符合 | $$32\geq 25$$, 符合 | $$30\geq25$$, 符合 | $$28\geq25$$, 符合 | **$$26\geq25$$, 符合** | $$24<25$$, 不符合 | $$22<25$$, 不符合 | $$20<25$$, 不符合 |

由上表知，當 $$x=3,~y=7$$ 唯一符合條件。則總價為$$350\times 3 + 200\times 7 =1050+1400=2450$$

> **[info] Tips**
>
> 從最有利的已知 $$x+y=10$$ 討論，因為是「等號」。
>
> 可觀察，第一組每次都「減3」；第二組每次都「減2」。



> **[warning] 注意**
>
> 不等式的解可能會有不止一組，最好都檢視過。

![第13題](https://imgur.com/mnPg29u.png)

**答**：

**解**：~~補圖~~

![第14題](https://imgur.com/RTp4HlW.png)

**答**：(D)

**解**：題目載明「每顆球抽到機會相等」，因此第53次時，箱內有55顆球，紅球有2顆，故所求 $$\frac2{55}$$

![第15題](https://imgur.com/pbe6gKl.png)

**答**：

**解**：~~補圖~~

![第16題](https://imgur.com/OjMDzmJ.png)

**答**：(B)

**解**：題目說「咖啡豆每公克價錢固定」，因此先找咖啡豆的單價

$$295 = 250 \times \text{每克價格} -5 \Rightarrow \text{每克價格}=\frac{300}{250}$$

因此阿嘉購買 $$x$$ 公克，需要支付 $$y=x\times \text{每克價格} = x\times \frac{300}{250}$$

![第17題](https://imgur.com/tn4aKW7.png)

**答**：

**解**：~~補圖~~

![第18題](https://imgur.com/wbmSaso.png)

**答**：(A)

**解**：先用小數字思考

1. 若只有 2 個車廂，則有 2 個間隔，每個間隔花費 $$30 \div 2 = 15$$ 分鐘
2. 若只有 3 個車廂，則有 3 個間隔，每個間隔花費 $$30 \div 3 = 10$$ 分鐘
3. 若有 36 個車廂，則有 36 個間隔，每個間隔花費 $$30 \div 36 = \frac56$$ 分鐘

而 21 號 與 9 號差距 $$21-9=12$$ 個堅格，需花費 $$\frac56 \times 12 = 10$$ 分鐘

![第19題](https://imgur.com/OdEcaV7.png)

**答**：

**解**：~~圖~~

![第20題](https://imgur.com/YN5hGv9.png)

**答**：(A)

**解**：設購買「均搭纜車」$$x$$ 人，購買「單程纜車、單程步行」$$y$$ 人，則

1. 共花費 $$300x+200y=4100 \Rightarrow 3x+2y=41$$
2. 所有人搭纜車的次數為 $$2x+y=15+10 \Rightarrow 2x+y=25$$

解聯立方程式 $$\begin{cases}3x+2y=41 \\ 2x+y=25 \end{cases}\Rightarrow x=9,~ y=7 $$，故總共 $$x+y=9+7=16$$ 人

![第21題](https://imgur.com/xKA99UD.png)

**答**：(A)

**解**：設 A 餐有 $$a$$ 份、B 餐有$$b$$ 份、C 餐有 $$c$$ 份，則

1. 總共有 $$10$$ 份義大利麵 $$ \Rightarrow a+b+c=10$$ 
2. 總共有 $$x$$ 杯飲料 $$\Rightarrow b+c=x$$
3. 總共有 $$y$$ 份沙拉 $$\Rightarrow c = y$$

解聯立方程式 $$\begin{cases}a+b+c=10\\ b+c=x \\ c=y\end{cases}\Rightarrow \begin{cases}a+b+y=10 \\ b+y=x \end{cases} \Rightarrow a+x=10 \Rightarrow a=10-x $$

![第22題](https://imgur.com/SBhPqVH.png)

**答**：(C)

**解**：由題可知，$$a$$ 為 $$35$$ 的倍數，設 $$a=35x$$，又 $$420 = 35 \times 12$$ 。則 $$(x,12)=1$$ （互質）

(A) 若 20 是 $$a$$ 的因數，則 $$x$$ 必為 $$2$$ 的倍數，則與 12 不互質，錯。

(B) 20不可能，錯。

(C) 若 25 是 $$a$$ 的因數，則 $$x$$ 必為 $$5$$ 的倍數，與 12 仍互質，對。

(D) 25可能，錯。

![第23題](https://imgur.com/ZzsbCqY.png)

**答**：

**解**：~~圖~~

![第24題](https://imgur.com/cRk5oZF.png)

**答**：

**解**：~~圖~~

![第25題](https://imgur.com/jtF9c79.png)

**答**：

**解**：~~圖~~

![第26題](https://imgur.com/zGfNw4Q.png)

**答**：(B)

**解**：由頂點 A ，設拋物線為 $$y=a(x+3)^2+0$$

因 $$\Delta ABC$$ 為正三角形，故可知 $$B$$ 點的 $$x$$ 坐標為 $$-3-\frac{2}{\sqrt3}$$，因此 $$B = (-3-\frac{2}{\sqrt3},2)$$

將 $$B$$ 代入 $$y=a(x+3)^2$$ ，得 $$2=a\times \frac43 , ~\Rightarrow a=\frac{3}{2}$$，則拋物線為 $$y=\frac{3}2(x+3)^2$$

設與 $$y$$ 軸交點 $$(0,m)$$ 代入拋物線，得 $$m=\frac32(0+3)^2 = \frac{27}2$$，故所求 $$(0,\frac{27}2)$$

## 第二部分：非選題（1~2題）

![非選1](https://imgur.com/Qstvx2M.png)

**答**：(1) 10 (2) 不合理

**解**：

(1) 由題目提供之公式 $$90\% = \frac{SPF-1}{SPF} \times 100\% \Rightarrow 0.9 SPF = SPF -1,~\therefore SPF=10$$

(2) 第一代防護率為 $$\frac{25-1}{25}=\frac{24}{25}$$，第二代防護率為 $$\frac{50-1}{50}=\frac{49}{50}$$，

​	則第二代 除以 第一代 為 $$\frac{49}{50} \times \frac{25}{24} = \frac{49}{48}=1.\text{多}\neq 2$$，故不合理！

![非選2](https://imgur.com/m9stS0E.png)

**答**：

**解**：~~補圖拉~~




# A note about Arduino Uno R3 + ESP8266

這裡簡單說一下將非WiFi版的 UNO R3 配合 ESP8266 的心路歷程。

在三個禮拜（超久）的探索之後，[這篇教學](https://maker.pro/esp8266/tutorial/esp8266-tutorial-how-to-control-anything-from-the-internet)最有用！

我要做的事情是利用ESP8266的連線功能，配合Line官方的 Line Notify 來傳訊息給自己。

以下教學～

需要準備的東西有：

1. Arduino Uno R3 (or any other proper replacement.)
2. ESP8266 (ESP-01)
3. BreadBoard
4. A lot of Dupont lines
5. Two buttons

`You have to be very careful that ESP8266 can just work on 3.3V. If the voltage is above it, you may blow it up! `

接線圖（參考至上方教學網址）：

![line](line.png)

可以注意到有一塊黑色的東東，可以忽略他！直接將板子的3.3V 跟 接地 接到麵包板上即可。

這裡再次強調，一定要用3.3V，若用到5V，就跟ESP8266講掰掰！

左邊的按鈕 RESET 代表重置。當你按了這顆按鈕，ESP8266就會重新啟動，就像Arduino的reset按鈕一樣。

右邊的FLASH 是可以切換FLASH mode，用處在於你要將程式燒進去的時後，要切換成flash mode，才能燒得進去。

到這邊後就萬事俱備只欠東風！



若你是第一次使用這塊板子，要先到你的Arduino IDE 的檔案/偏好設定 (File/Preference) 然後在底下輸入網址：

**http://arduino.esp8266.com/stable/package_esp8266com_index.json**

並且把Debug mode開啟，這樣子你在燒的時候比較知道狀況。



接著，選擇開發版，我選擇的是Generic ESP8266 module。



再來，先試試看一個範例程式（參考至上方教學網站）

```C
#include <ESP8266WiFi.h>

const char* ssid = "YOUR_SSID";//type your ssid
const char* password = "YOUR_PASSWORD";//type your password

int ledPin = 2; // GPIO2 of ESP8266
WiFiServer server(80);//Service Port

void setup() {
Serial.begin(115200);
delay(10);

pinMode(ledPin, OUTPUT);
digitalWrite(ledPin, LOW);

// Connect to WiFi network
Serial.println();
Serial.println();
Serial.print("Connecting to ");
Serial.println(ssid);

WiFi.begin(ssid, password);

while (WiFi.status() != WL_CONNECTED) {
delay(500);
Serial.print(".");
}
Serial.println("");
Serial.println("WiFi connected");

// Start the server
server.begin();
Serial.println("Server started");

// Print the IP address
Serial.print("Use this URL to connect: ");
Serial.print("http://");
Serial.print(WiFi.localIP());
Serial.println("/");
}

void loop() {
// Check if a client has connected
WiFiClient client = server.available();
if (!client) {
return;
}

// Wait until the client sends some data
Serial.println("new client");
while(!client.available()){
delay(1);
}

// Read the first line of the request
String request = client.readStringUntil('\r');
Serial.println(request);
client.flush();

// Match the request

int value = LOW;
if (request.indexOf("/LED=ON") != -1) {
digitalWrite(ledPin, HIGH);
value = HIGH;
} 
if (request.indexOf("/LED=OFF") != -1){
digitalWrite(ledPin, LOW);
value = LOW;
}

//Set ledPin according to the request
//digitalWrite(ledPin, value);

// Return the response
client.println("HTTP/1.1 200 OK");
client.println("Content-Type: text/html");
client.println(""); //  do not forget this one
client.println("<!DOCTYPE HTML>");
client.println("<html>");

client.print("Led pin is now: ");

if(value == HIGH) {
client.print("On");  
} else {
client.print("Off");
}
client.println("<br><br>");
client.println("Click <a href=\"/LED=ON\">here</a> turn the LED on pin 2 ON<br>");
client.println("Click <a href=\"/LED=OFF\">here turn the LED on pin 2 OFF<br>");
client.println("</html>");

delay(1);
Serial.println("Client disconnected");
Serial.println("");
}
```

將SSID 跟 PASSWORD 改成你的WiFi就可以順利連線囉！

這個程式的目的在於連線到WiFi之後，在內網開啟一個WebServer，簡單來說就是你可以「在同一個wifi」的時候，透過網址連線到板子做的網站。而這個網站可以接收你的動作，因此就可以拿來控制板子。

但我要做的是讓板子連線到網路後，主動傳訊息給我，因此等等我不會繼續用這個範例。這邊請開啟序列埠，將baud rate 調整到115200，然後看有沒有出現IP，應該會是192.168.x.x的樣子，若有，說明連線成功！



這邊請特別注意燒程式的方法！

按「上傳」之後，會有兩個過程，「草稿編譯」跟「上傳」。

等到你的IDE顯示「上傳中」的時候，要趕快「先按FLASH，按住不放」「按下RESET」「放開RESET」「放開FLASH」，

過程不要太快，一個一個慢慢按。一開始還沒抓到訣竅的時候可能很容易失敗。成功的話會看到ESP8266瘋狂閃藍燈。



好了，再來我們要做的事情是跟Line Notify結合。

首先要到Line Noify 申請一個權杖，這邊我之後再補教學（若有想到啦）。直接進入程式部分：

```C
#include <ESP8266WiFi.h>
#include <WiFiClientSecureAxTLS.h>

#define WIFI_SSID "你的wifi名稱"
#define WIFI_PASSWORD "你的wifi密碼"
#define LINE_TOKEN "你的權杖"

String msg = "test";

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("連線中");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.print("\n 分配到 IP: ");
  Serial.println(WiFi.localIP());
}

void loop() {

  WiFiClientSecure client;
  client.setInsecure();
  if (!client.connect("notify-api.line.me", 443)) {
    Serial.println("連線失敗!!!");
    Serial.println(client.status());
    delay(5000);
    return;
  }
  msg = getUVMessage();
  String  req = "POST /api/notify HTTP/1.1\r\n";
  req += "Host: notify-api.line.me\r\n";
  req += "Authorization: Bearer " + String(LINE_TOKEN) + "\r\n";
  req += "Cache-Control: no-cache\r\n";
  req += "User-Agent: ESP8266\r\n";
  req += "Connection: close\r\n";
  req += "Content-Type: application/x-www-form-urlencoded\r\n";
  req += "Content-Length: " + String(String("message=" + msg).length()) + "\r\n";
  req += "\r\n";
  req += "message=" + msg ;
  client.print(req);

  while (client.connected()) {
    String line = client.readStringUntil('\n');
    if (line == "\r")
      break;
 	 }
	}
}
```



在第八行的地方，宣告了一個msg，

其他地方都不要改，只要之後將msg改成你要的訊息，就可以了！

請注意到第27行非常關鍵，我卡這一步卡三個禮拜Ｑ

加了 `client.setInsecure()`，才能順利連線到別的網站！否則會一直連線失敗！



好了，到這邊就是將ESP8266燒一個Line Notify的過程。我所傳的都是簡略版的範例程式，目的是比較好理解跟修改，讀者再根據自身需求來修改成自己需要的功能！



**2020.12.30 林冠曄**


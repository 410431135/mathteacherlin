# 物質的基本結構

> Author : 林冠曄
>
> First edition : 2019/12/18
>
> Last update : 2019/12/18

## 元素和化合物

物質可分為「純物質」、「混和物」，純物質中可細分為「元素」、「化合物」。

- 「元素」為單一種原子所構成之可表現性質的純物質。
	- 其可能為一堆一起出現（如金屬）、單一個原子出現（單原子分子）、成雙成對的出現（雙原子分子）。
	- 例如：鎂（金屬）、鐵（金屬）、氧氣（雙原子分子）、氬氣（單原子分子）。
	- 元素無法用一般方式再分解為更細小的單位。
		- 例如無法將氧氣 $$O_2$$ 分解為 兩個氧原子。
- 「化合物」為兩種以上的元素所組成（化合）。
	- 同一化合物的元素組成比例一定固定。（道爾頓的原子說）
	- 例如：一個水分子以一個氧原子、兩個氫原子所組成。
	- 可以用一般的方式再分解為更細小的單位（元素）。
		- 例如普利士力將氧化汞變為汞+氧氣 $$2HgO \rightarrow 2Hg+O_2$$
- 「混和物」為兩種以上「純物質」所混合成。
	- 沒有特定的比例及固定的性質。

[普力士力實驗](https://www.youtube.com/watch?v=_Y1alDuXm6A)

<iframe width="560" height="315" src="https://www.youtube.com/embed/_Y1alDuXm6A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

`2019/12/18`

## 生活中常見的元素

[Melting Diamonds with Oxygen](https://www.youtube.com/watch?v=YbQbEOblGYE)

<iframe width="560" height="315" src="https://www.youtube.com/embed/YbQbEOblGYE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>